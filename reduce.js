let reduce = (elements, cb, acc) => {
    let startValue = acc;
    if (!elements || !cb || !Array.isArray(elements)) {
        return [];
    }
    else {

        if (startValue === undefined) {
            startValue = elements[0]
        }
        else {
            startValue = cb(startValue, elements[0], elements)
        }
        for (let index = 1; index < elements.length; index++) {

            startValue = cb(startValue, elements[index], index, elements);

        }
        return startValue;
    }
}
module.exports = reduce;