let flatten = (elements, depth) => {
    if (depth === undefined) {
        depth = 1;
    }
    if (!elements || !Array.isArray(elements)) {
        return [];
    }
    else {

        const result = [];
        for (let index = 0; index < elements.length; index++) {
            if (Array.isArray(elements[index]) && depth > 0) {
                result.push(...flatten(elements[index], depth - 1));
            } else if (elements[index] == undefined) {
                continue;
            }
            else {
                result.push(elements[index]);
            }
        }
        return result;
    }

}

module.exports = flatten;
