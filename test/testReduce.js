const array = require('../item');
const reduce = require('../reduce');

let acc = undefined;

const cb = (acc, index, value, array) => acc + index;
const result = reduce(array, cb, acc);

console.log(result);