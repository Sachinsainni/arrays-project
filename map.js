let map = (elements, cb) => {


    if (!elements || !cb || !Array.isArray(elements)) {
        return [];
    }


    else {

        let result = [];

        for (let index = 0; index < elements.length; index++) {
            result.push(cb(elements[index], index, elements));
        }

        return result;
    }
}

module.exports = map;