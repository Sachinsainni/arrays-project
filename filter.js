let filter = (array, cb) => {
  if (!array || !cb || !Array.isArray(array)) {
    return [];
  }
  else {
    const sendBack = [];
    for (let index = 0; index < array.length; index++) {
      if (cb(array[index], index, array) == true) {

        sendBack.push(array[index]);
      }
    }
    return sendBack;

  }
}
module.exports = filter;