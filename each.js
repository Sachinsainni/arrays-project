const each = (elements, cb) => {
    let result = [];
    for (let index = 0; index < elements.length; index++) {
        
       result.push(cb(elements[index]));
    }
    return result;
}
module.exports = each;